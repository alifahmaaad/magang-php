use `consumer complaints`;
select month(`date_received`) 'Month' ,count(`complaint_id`) 'Total Complaints'
     from consumer_complaints
     where `complaint_id` is not null
     group by month(`date_received`)
     order by month(`date_received`);