use `consumer complaints`;
SELECT company, sum(case when `company_response_to_consumer` = 'closed' then 1 else 0 end) AS Closed, sum(case when `company_response_to_consumer` = 'Closed with explanation' then 1 else 0 end) AS 'Closed with explanation', sum(case when `company_response_to_consumer` = 'Closed with non-monetary relief' then 1 else 0 end) AS 'Closed with non-monetary relief'
FROM consumer_complaints GROUP BY company order by company;
