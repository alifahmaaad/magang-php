<?php

namespace App\Exports;

use App\Models\Employee;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class EmployeesExport implements FromView
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function view(): View
    {
        $employees = Employee::join('companies', 'employees.company_id', '=', 'companies.id')->leftJoin('employees as atasan', 'atasan.id', '=', 'employees.atasan_id')->get(['employees.*', 'atasan.nama as atasan', 'companies.nama as namacomp']);
        return view('exports.exportemployee', [
            'employees' => $employees
        ]);
    }
}
