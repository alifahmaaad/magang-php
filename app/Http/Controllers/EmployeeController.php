<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\Employee;
use App\Exports\EmployeesExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = Employee::join('companies', 'employees.company_id', '=', 'companies.id')->leftJoin('employees as atasan', 'atasan.id', '=', 'employees.atasan_id')->get(['employees.*', 'atasan.nama as atasan', 'companies.nama as namacomp']);
        return view('employee.employee', compact('employees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $employees = Employee::all();
        $companies = Company::all();
        return view('employee.createemployee', compact(['employees', 'companies']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'Nama' => 'required',
            'company_id' => 'required'
        ]);
        Employee::create([
            'nama' => $request->Nama,
            'atasan_id' => $request->atasan_id,
            'company_id' => $request->company_id
        ]);
        return redirect()->back()->with('success', 'Employee created!');
    }
    public function export()
    {
        return Excel::download(new EmployeesExport, 'EmployeesExport.xlsx');
    }
    public function exportpdf()
    {
        return Excel::download(new EmployeesExport, 'EmployeesExport.pdf');
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $theemployee = Employee::findorfail($id);
        $employees = Employee::all();
        $companies = Company::all();
        return view('Employee.editEmployee', compact(['theemployee', 'employees', 'companies']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'Nama' => 'required',
            'atasan_id' => 'required',
            'company_id' => 'required'
        ]);
        Employee::where('id', $id)
            ->update([
                'nama' => $request->Nama,
                'atasan_id' => $request->atasan_id,
                'company_id' => $request->company_id
            ]);
        return redirect()->back()->with('success', 'Employee edited!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Employee::where('id', $id)->forceDelete();
        return redirect()->back()->with('success', 'employee deleted!');
    }
}
