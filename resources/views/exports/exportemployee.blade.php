<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <title>Export Table</title>
</head>

<body>
    <table class="table">
        <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Nama</th>
                <th scope="col">Posisi</th>
                <th scope="col">Perusahaan</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($employees as $employee)
            <tr>
                <td>{{ $employee->id }}</td>
                <td>{{$employee->nama}}</td>
                <td>@if ($employee->atasan_id =='' or $employee->atasan_id ==null)
                    CEO
                    @elseif($employee->atasan_id ==1)
                    Direktur
                    @elseif($employee->atasan_id > 1 and ($employee->atasan_id <=3)) Manager @else Staff @endif <td>{{$employee->namacomp}}</td>
            </tr>
            @endforeach

        </tbody>
    </table>
</body>

</html>