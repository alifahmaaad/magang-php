@include('navbar')
<div class="container">
    <div class="row text-center py-2">
        <h2>Create Employee</h2>
    </div>
    <form action="{{ url('employee/store') }}" method="post" enctype="multipart/form-data">
        @csrf

        @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif
        @if (session('failed'))
        <div class="alert alert-danger">
            {{ session('failed') }}
        </div>
        @endif
        <input class="form-control my-3 " type="text" id="Nama" name="Nama" placeholder="Nama" aria-label="default input example" maxlength="25">

        <select class="form-select my-3 " aria-label="Default select example" id="atasan_id" name="atasan_id">
            <option value="" disabled selected>--- Select Atasan ---</option>
            <option value="">NULL</option>
            @foreach ($employees as $employee)
            <option value="{{$employee->id}}">{{$employee->nama}}</option>
            @endforeach
        </select>


        <select class="form-select my-3 " aria-label="Default select example" id="company_id" name="company_id">
            <option value="" disabled selected>--- Select Company ---</option>
            @foreach ($companies as $comp)
            <option value="{{$comp->id}}">{{$comp->nama}}</option>
            @endforeach
        </select>

        <div class="form-group mt-3">
            <button class="btn btn-primary btn-block">Create</button>
        </div>

    </form>
</div>