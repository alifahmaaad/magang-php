@include('navbar')
<div class="container">
    <div class="py-3">
        <a href="{{ url('employee/create') }}" type="button" class="btn btn-success">Create</a>
        <a href="{{ url('employee/exportpdf') }}" type="button" class="btn btn-secondary">Export PDF</a>
        <a href="{{ url('employee/exportexcel') }}" type="button" class="btn btn-secondary">Export Excel</a>
    </div>
    <div class="row text-center py-2">
        <h2>Employee</h2>
    </div>
    <div class="row">
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">No</th>
                    <th scope="col">Nama</th>
                    <th scope="col">Atasan</th>
                    <th scope="col">Company</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($employees as $employee)
                <tr>
                    <th scope="col">{{$loop->iteration}}</th>
                    <td>{{$employee->nama}}</td>
                    <td>{{$employee->atasan}}</td>
                    <td>{{$employee->namacomp}}</td>
                    <td>
                        <div class="btn-group" role="group" aria-label="Basic outlined button group">
                            <a type="button" class="btn btn-primary" href="{{ url('employee/' . $employee->id . '/edit') }}">Edit</a>
                            <a type="button" class="btn btn-danger" href="{{ url('employee/' . $employee->id . '/delete') }}">Delete</a>
                        </div>
                    </td>
                </tr>

                @endforeach
            </tbody>
        </table>
    </div>
</div>
</body>

</html>