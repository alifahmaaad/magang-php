@include('navbar')
<div class="container">
    <div class="row text-center py-2">
        <h2>Edit Employee</h2>
    </div>
    <form action="{{ url('employee/.$theemployee->id./update') }}" method="post" enctype="multipart/form-data">
        @csrf

        @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif
        @if (session('failed'))
        <div class="alert alert-danger">
            {{ session('failed') }}
        </div>
        @endif


        <input class="form-control my-3 " type="text" id="Nama" name="Nama" placeholder="Nama" value="{{$theemployee->nama}}" aria-label="default input example" maxlength="25">

        <select class="form-select my-3 " aria-label="Default select example" id="atasan_id" name="atasan_id">
            <option value="$theemployee->atasan_id" selected>
                @foreach ($employees as $employee)
                @if($theemployee->atasan_id == $employee->id)
                {{$employee->nama}}
                @endif
                @endforeach
            </option>
            <option value="">NULL</option>
            @foreach ($employees as $employee)
            <option value="{{$employee->id}}">{{$employee->nama}}</option>
            @endforeach
        </select>


        <select class="form-select my-3 " aria-label="Default select example" id="company_id" name="company_id">
            <option value="$theemployee->company_id" selected>
                @foreach ($companies as $comp)
                @if($theemployee->company_id == $comp->id)
                {{$comp->nama}}
                @endif
                @endforeach
            </option>
            @foreach ($companies as $comp)
            <option value="{{$comp->id}}">{{$comp->nama}}</option>
            @endforeach
        </select>
        <div class="form-group mt-3">
            <button class="btn btn-primary btn-block">Edit</button>
        </div>

    </form>
</div>