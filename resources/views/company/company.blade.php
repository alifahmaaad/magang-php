@include('navbar')
<div class="container">
    <div class="py-3">
        <a href="{{ url('company/create') }}" type="button" class="btn btn-success">Create</a>
    </div>
    @if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
    @endif
    @if (session('failed'))
    <div class="alert alert-danger">
        {{ session('failed') }}
    </div>
    @endif
    <div class="row text-center py-2">
        <h2>Company</h2>
    </div>
    <div class="row">
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">No</th>
                    <th scope="col">Nama</th>
                    <th scope="col">Alamat</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($companys as $company)
                <tr>
                    <th scope="col">{{$loop->iteration}}</th>
                    <td>{{$company->nama}}</td>
                    <td>{{$company->alamat}}</td>
                    <td>
                        <div class="btn-group" role="group" aria-label="Basic outlined button group">
                            <a type="button" class="btn btn-primary" href="{{ url('company/' . $company->id . '/edit') }}">Edit</a>
                            <a type="button" class="btn btn-danger" href="{{ url('company/' . $company->id . '/delete') }}">Delete</a>
                        </div>
                    </td>
                </tr>

                @endforeach
            </tbody>
        </table>
    </div>
</div>
</body>

</html>