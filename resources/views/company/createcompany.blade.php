@include('navbar')
<div class="container">
    <div class="row text-center py-2">
        <h2>Create Company</h2>
    </div>
    <form action="{{ url('company/store') }}" method="post" enctype="multipart/form-data">
        @csrf

        @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif
        @if (session('failed'))
        <div class="alert alert-danger">
            {{ session('failed') }}
        </div>
        @endif
        <input class="form-control my-3 " type="text" id="Nama" name="Nama" placeholder="Nama" aria-label="default input example" maxlength="255">
        <input class="form-control my-3 " type="text" id="Alamat" name="Alamat" placeholder="Alamat" aria-label="default input example" maxlength="255">
        <div class="form-group mt-3">
            <button class="btn btn-primary btn-block">Create</button>
        </div>
    </form>
</div>