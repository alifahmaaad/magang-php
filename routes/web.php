<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\EmployeeController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [EmployeeController::class, 'index']);

//Company
Route::get('/company', [CompanyController::class, 'index']);
Route::get('/company/create', [CompanyController::class, 'create']);
Route::post('/company/store', [CompanyController::class, 'store']);
Route::get('/company/{id}/delete', [CompanyController::class, 'destroy']);
Route::get('/company/{id}/edit', [CompanyController::class, 'edit']);
Route::post('/company/{id}/update', [CompanyController::class, 'update']);

//Employee
Route::get('/employee', [EmployeeController::class, 'index']);
Route::get('/employee/create', [EmployeeController::class, 'create']);
Route::post('/employee/store', [EmployeeController::class, 'store']);
Route::get('/employee/{id}/delete', [EmployeeController::class, 'destroy']);
Route::get('/employee/{id}/edit', [EmployeeController::class, 'edit']);
Route::post('/employee/{id}/update', [EmployeeController::class, 'update']);
Route::get('/employee/exportexcel', [EmployeeController::class, 'export']);
Route::get('/employee/exportpdf', [EmployeeController::class, 'exportpdf']);
