-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 14 Jun 2021 pada 13.03
-- Versi server: 10.4.13-MariaDB
-- Versi PHP: 7.3.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sdm`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `companies`
--

CREATE TABLE `companies` (
  `id` bigint(11) NOT NULL,
  `nama` varchar(25) DEFAULT NULL,
  `alamat` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `companies`
--

INSERT INTO `companies` (`id`, `nama`, `alamat`) VALUES
(1, 'PT JAVAN ', 'Sleman'),
(2, 'PT Dicoding', 'Bandung');

-- --------------------------------------------------------

--
-- Struktur dari tabel `departemen`
--

CREATE TABLE `departemen` (
  `id` bigint(11) NOT NULL,
  `nama` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `departemen`
--

INSERT INTO `departemen` (`id`, `nama`) VALUES
(1, 'Manajemen'),
(2, 'Pengembangan Bisnis'),
(3, 'Teknisi'),
(4, 'Analis');

-- --------------------------------------------------------

--
-- Struktur dari tabel `employees`
--

CREATE TABLE `employees` (
  `id` bigint(11) NOT NULL,
  `nama` varchar(25) DEFAULT NULL,
  `atasan_id` bigint(11) DEFAULT NULL,
  `company_id` bigint(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `employees`
--

INSERT INTO `employees` (`id`, `nama`, `atasan_id`, `company_id`) VALUES
(1, 'Pak Budi', NULL, 1),
(2, 'Pak Tono', 1, 1),
(3, 'Pak Totok', 1, 1),
(4, 'Bu Sinta', 2, 1),
(5, 'Bu Novi', 3, 1),
(6, 'Andre', 4, 1),
(7, 'Dono', 4, 1),
(8, 'Ismir', 5, 1),
(9, 'Anto', 5, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `karyawan`
--

CREATE TABLE `karyawan` (
  `id` bigint(11) NOT NULL,
  `nama` varchar(25) NOT NULL,
  `jenis_kelamin` char(1) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `tanggal_masuk` date DEFAULT NULL,
  `departemen` bigint(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `karyawan`
--

INSERT INTO `karyawan` (`id`, `nama`, `jenis_kelamin`, `status`, `tanggal_lahir`, `tanggal_masuk`, `departemen`) VALUES
(1, 'Rizki Saputra', 'L', 'Menikah', '1980-10-11', '2011-01-01', 1),
(2, 'Farhan Reza', 'L', 'Menikah', '1989-11-01', '2011-01-01', 1),
(3, 'Riyando Adi', 'L', 'Menikah', '1977-01-25', '2011-01-01', 1),
(5, 'Satya Laksana', 'L', 'Menikah', '1981-01-12', '2011-03-19', 2),
(6, 'Miguel Hernandez', 'L', 'Menikah', '1994-10-16', '2014-06-15', 2),
(7, 'Putri Persada', 'P', 'Menikah', '1988-01-30', '2013-04-14', 2),
(9, 'Haqi Hafiz', 'L', 'Belum', '1995-09-19', '2015-03-09', 3),
(10, 'Abi Isyawara', 'L', 'Menikah', '1991-06-03', '2012-01-22', 3),
(12, 'Nadia Aulia', 'P', 'Menikah', '1989-10-07', '2012-05-07', 4),
(13, 'Mutiara Rezki', 'P', 'Menikah', '1988-03-23', '2013-05-21', 4),
(14, 'Dani Setiawan', 'L', 'Menikah', '1986-02-11', '2014-11-30', 4),
(15, 'Budi Putra', 'L', 'Menikah', '1995-10-23', '2015-12-03', 4);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `departemen`
--
ALTER TABLE `departemen`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employee_company__fk` (`company_id`);

--
-- Indeks untuk tabel `karyawan`
--
ALTER TABLE `karyawan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `karyawan_departemen__fk` (`departemen`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `companies`
--
ALTER TABLE `companies`
  MODIFY `id` bigint(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `departemen`
--
ALTER TABLE `departemen`
  MODIFY `id` bigint(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `employees`
--
ALTER TABLE `employees`
  MODIFY `id` bigint(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `karyawan`
--
ALTER TABLE `karyawan`
  MODIFY `id` bigint(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `employees`
--
ALTER TABLE `employees`
  ADD CONSTRAINT `employee_company__fk` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`);

--
-- Ketidakleluasaan untuk tabel `karyawan`
--
ALTER TABLE `karyawan`
  ADD CONSTRAINT `karyawan_departemen__fk` FOREIGN KEY (`departemen`) REFERENCES `departemen` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
